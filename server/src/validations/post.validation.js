const Joi = require('joi');

const createPost = Joi.object({
  location: Joi.string(),
  author: Joi.string(),
  id: Joi.number(),
  time: Joi.number(),
  text: Joi.string(),
});

const updatePost = Joi.object({
  location: Joi.string(),
  author: Joi.string(),
  id: Joi.number(),
  time: Joi.number(),
  week: Joi.number(),
  text: Joi.string(),
});


module.exports = {
  createPost,
  updatePost,
};
