const dayjs = require('dayjs');
var weekOfYear = require('dayjs/plugin/weekOfYear')
dayjs.extend(weekOfYear);

// Utility to add a week value to post based on date timestamp
const decorateWithWeek = (posts) => {
  let sortedPosts = [];
  sortedPosts = posts.sort((x, y) => {
    return x.time - y.time;
  })

  sortedPosts.forEach(p => {
    p.week = dayjs(p.time).week();
  })
  return sortedPosts;
};

module.exports = {
  decorateWithWeek
};