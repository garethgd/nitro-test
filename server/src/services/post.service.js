const httpStatus = require('http-status');
const fs = require('fs');
const { decorateWithWeek } = require('../utils/decorateWithWeek');
const ApiError = require('../utils/ApiError');
const samplePosts = require('../data/sampleData.json');
const path = require('path');

const createPost = (postBody) => {
  if (!postBody) {
    throw new ApiError(httpStatus.NOT_FOUND, 'No post sent.');
  }
  samplePosts.push(postBody);
  fs.writeFileSync(path.join(__dirname, '../data/sampleData.json'), JSON.stringify(samplePosts));
  return decorateWithWeek(samplePosts);
};

const getPosts = async () => {
  // with no db component using locale file reading 
  // to get post data
  // otherwise this would be a mongoose Model read
  let posts = decorateWithWeek(samplePosts);
  return posts;
};

const getPostById = (id) => {
  if (!id) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Unable to lookup post.');
  }
  const matchedPost = samplePosts.find(post => post.id === parseInt(id));
  return matchedPost;
};

const updatePost = async (updatedPost, id) => {
  if (!updatedPost) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Post not found');
  }
  const matchedIndex = samplePosts.findIndex((post) => post.id === parseInt(id));

  if (matchedIndex !== -1) {
    samplePosts[matchedIndex] = updatedPost
  }

  fs.writeFileSync(path.join(__dirname, '../data/sampleData.json'), JSON.stringify(samplePosts));
  return updatedPost;
};

module.exports = {
  createPost,
  getPostById,
  updatePost,
  getPosts,
};
