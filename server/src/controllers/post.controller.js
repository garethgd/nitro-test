const catchAsync = require('../utils/catchAsync');

// I have a catchAsync utility that acts as wrapping try / catch
// but since we are reading from a local file its not up to much
// however I just wanted to include it to show how I'd handle try/ catch

const { postService } = require('../services');

const getPosts = catchAsync(async (req, res) => {
  const result = await postService.getPosts();
  res.send(result);
});

const updatePost = catchAsync(async (req, res) => {
  const postId = req.params.postId;
  const updatedPost = req.body;
  postService.updatePost(updatedPost, postId);
  res.send(updatedPost);
});

const createPost = catchAsync((req, res) => {
  const createdPost = req.body;
  const result = postService.createPost(createdPost);
  res.send(result)
});

module.exports = {
  getPosts,
  createPost,
  updatePost
};
