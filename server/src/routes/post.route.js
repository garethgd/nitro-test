const express = require('express');
const postController = require('../controllers/post.controller');
const postValidation = require('../validations/post.validation');
const validator = require('express-joi-validation').createValidator({})

const router = express.Router();
router.route('/').post(validator.body(postValidation.createPost), postController.createPost).get(postController.getPosts);
router.route('/:postId').put(validator.body(postValidation.updatePost), postController.updatePost);

module.exports = router;
