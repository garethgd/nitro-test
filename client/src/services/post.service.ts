import axios from 'axios'
import { config } from '../config/config'
import type { Post } from '../types/Post'
const apiUrl = config.api.url

const getPosts = async (): Promise<Post[]> => {
  const getPostsReq = await axios.get<Post[]>(`${apiUrl}/posts`, {});
  const posts = getPostsReq.data;
  return posts;
};

const createPost = async (post: Post): Promise<Post[]> => {
  const createPostReq = await axios.post<Post[]>(`${apiUrl}/posts`, post);
  const createdPost = createPostReq.data;
  return createdPost;
};

const updatePost = async (post: Post): Promise<Post> => {
  const { id } = post;
  const updatePostReq = await axios.put<Post>(`${apiUrl}/posts/${id}`, post);
  const updatedPost = updatePostReq.data;
  return updatedPost;
};


export const postService = {
  getPosts,
  createPost,
  updatePost
}