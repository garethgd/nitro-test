import { Post } from '../../../types/Post';
import { Action } from '../../actions/types';
import { ActionTypes } from '../../actions/constants';

export interface State {
  posts: Post[];
  isLoading: boolean;
  groupId: keyof Post;
  isAddModalOpen: boolean;
}

export const initalState: State = {
  posts: [],
  isLoading: true,
  isAddModalOpen: false,
  groupId: 'week',
};

export const reducer = (state = initalState, action: Action) => {
  switch (action.type) {
    case ActionTypes.IS_BUSY: {
      return { ...state, isLoading: action.payload };
    }
    case ActionTypes.CREATE_POST: {
      const posts = action.payload;
      return {
        ...state,
        posts,
        isAddModalOpen: false,
      };
    }
    case ActionTypes.GET_POSTS: {
      const posts = action.payload;
      return { ...state, posts, isLoading: false };
    }
    case ActionTypes.TOGGLE_ADD_MODAL: {
      const isAddModalOpen = state.isAddModalOpen;
      return { ...state, isLoading: false, isAddModalOpen: !isAddModalOpen };
    }
    case ActionTypes.SET_GROUP_ID: {
      const groupId = action.payload;
      return { ...state, groupId };
    }
    case ActionTypes.UPDATE_POST: {
      const updatedPost = action.payload;
      const posts = state.posts;

      const updatedPosts = posts.map(post => {
        if (post.id === updatedPost.id) {
          return updatedPost;
        } else return post;
      });

      return {
        ...state,
        posts: updatedPosts,
        isLoading: false,
      };
    }

    default:
      return state;
  }
};