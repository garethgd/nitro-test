import { combineReducers } from 'redux'
import * as blog from './blog';
import * as info from './info'

export interface State {
  blog: blog.State,
  info: info.State
}

/*
 * initialState of the app
 */

export const initialState: State = {
  blog: blog.initalState,
  info: info.initalState
}

/*
  Root reducer
 */
export const reducer = combineReducers<State>({
  blog: blog.reducer,
  info: info.reducer
} as any)