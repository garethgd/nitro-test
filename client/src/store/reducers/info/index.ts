import { Action } from '../../actions/types';
import { ActionTypes } from '../../actions/constants';
export interface State {
  isLoading: boolean;
  snackbar: {
    message: string;
    open: boolean;
  };
}

export const initalState: State = {
  isLoading: true,
  snackbar: {
    message: '',
    open: false,
  },
};

export const reducer = (state = initalState, action: Action) => {
  switch (action.type) {
    case ActionTypes.IS_BUSY: {
      return { ...state, isLoading: action.payload };
    }

    case ActionTypes.GET_POSTS: {
      return { ...state, isLoading: false };
    }

    case ActionTypes.TOGGLE_SNACKBAR: {
      const isSnackbarOpen = state.snackbar.open;
      const snackbarMsg = action.payload.message;
      return {
        ...state,
        isLoading: false,
        snackbar: {
          message: snackbarMsg,
          open: !isSnackbarOpen
        }
      };
    }

    case ActionTypes.FAILED_REQUEST: {
      return {
        ...state,
        isLoading: false,
        snackbar: { message: action.payload, open: true },
      };
    }

    default:
      return state;
  }
};