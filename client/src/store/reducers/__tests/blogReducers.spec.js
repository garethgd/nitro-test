import * as blog from '../blog';
import { ActionTypes } from '../../actions/constants';
// Test reducers for no side effects

describe('blog reducer tests', () => {
  it('returns inital state correctly', () => {
    expect(blog.reducer(undefined, {})).toEqual(blog.initalState);
  });

  const testPosts = [
    {
      "id": 3,
      "location": "San Francisco",
      "time": "1552571174",
      "author": "Happy Developer",
      "text": "Digital transformation isn’t just a buzzword"
    },
    {
      "id": 4,
      "location": "Sydney",
      "time": "1552563973",
      "author": "Happy Developer",
      "text": "An expectation of digital efficiency has become the norm in our daily lives"
    },
  ];

  const newPost = {
    "id": 5,
    "location": "Dublin",
    "time": "1553080742",
    "author": "Happy Manager",
    "text": "A modern PDF annotator that can accommodate all of the cooks in a very busy kitchen is what your employees really need."
  };

  const customInitState = Object.assign({}, blog.initalState);
  customInitState.posts = testPosts;

  it('should create a post', () => {
    const allPostsWithNewlyAdded = [newPost, ...customInitState.posts]
    expect(
      blog.reducer(customInitState, {
        type: ActionTypes.CREATE_POST,
        payload: allPostsWithNewlyAdded,
      })
    ).toEqual({
      ...customInitState,
      posts: allPostsWithNewlyAdded,
    });
  });

  it('should group by id', () => {
    expect(
      blog.reducer(customInitState, {
        type: ActionTypes.SET_GROUP_ID,
        payload: 'location',
      })
    ).toEqual({
      ...customInitState,
      groupId: 'location',
    });
  });

  const editedPost = {
    "id": 4,
    "location": "Dublin",
    "time": "1553099742",
    "author": "Happy Manager",
    "text": "An integrated productivity solution breaks information through barriers and allows workers to collaborate in real time."
  };

  it('should get all posts', () => {
    expect(
      blog.reducer(blog.initalState, {
        type: ActionTypes.GET_POSTS,
        payload: testPosts,
      })
    ).toEqual({
      ...customInitState,
      isLoading: false,
      posts: testPosts,
    });
  });

  it('should update post', () => {
    expect(
      blog.reducer(customInitState, {
        type: ActionTypes.UPDATE_POST,
        payload: editedPost,
      })
    ).toEqual({
      ...customInitState,
      isLoading: false,
      posts: [
        {
          "id": 3,
          "location": "San Francisco",
          "time": "1552571174",
          "author": "Happy Developer",
          "text": "Digital transformation isn’t just a buzzword"
        },
        {
          "id": 4,
          "location": "Dublin",
          "time": "1553099742",
          "author": "Happy Manager",
          "text": "An integrated productivity solution breaks information through barriers and allows workers to collaborate in real time."
        },
      ],

    });
  });

});
