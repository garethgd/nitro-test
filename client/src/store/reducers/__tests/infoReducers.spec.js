import * as info from '../info';
import { ActionTypes } from '../../actions/constants';

// Test reducers for no side effects
describe('info reducer tests', () => {
  it('returns inital state correctly', () => {
    expect(info.reducer(undefined, {})).toEqual(info.initalState);
  });

  const customInitState = Object.assign({}, info.initalState);

  it('should update snackbar when dispatched', () => {
    expect(
      info.reducer(customInitState, {
        type: ActionTypes.TOGGLE_SNACKBAR,
        payload: { message: 'Test Message' },
      })
    ).toEqual({
      ...info.initalState,
      isLoading: false,
      snackbar: {
        message: 'Test Message',
        open: true,
      },
    });
  });

  it('should handle request failed', () => {
    expect(
      info.reducer(info.initalState, {
        type: ActionTypes.FAILED_REQUEST,
        payload: 'Author Required',
      })
    ).toEqual({
      ...info.initalState,
      isLoading: false,
      snackbar: {
        message: 'Author Required',
        open: true,
      },
    });
  });
});
