import logger from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'
import { State, reducer, initialState } from './reducers'
import thunkMiddleware from 'redux-thunk';

// Seem to be an issue here with new redux types, had to use any to fit type requirements
const store = createStore<State, any, any, any>(reducer, initialState, applyMiddleware(thunkMiddleware, logger))

export default store