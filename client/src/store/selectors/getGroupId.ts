import type { State } from '../reducers'

type PostState = State['blog'];
export const getGroupId = (state: State): PostState['groupId'] => {
  return state.blog.groupId;
};

export default getGroupId;
