import type { State } from '../reducers'

type PostState = State['info'];
export const getIsLoading = (state: State): PostState['isLoading'] => {
  return state.info.isLoading;
};

export default getIsLoading;
