import type { State } from '../reducers'

type PostState = State['blog']['posts'];
export const getPosts = (state: State): PostState => {
  return state.blog.posts;
};

export default getPosts;
