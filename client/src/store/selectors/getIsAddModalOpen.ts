import type { State } from '../reducers'

type PostState = State['blog'];
export const getIsAddModalOpen = (state: State): PostState['isAddModalOpen'] => {
  return state.blog.isAddModalOpen;
};

export default getIsAddModalOpen;
