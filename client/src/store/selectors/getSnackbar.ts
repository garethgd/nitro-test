import type { State } from '../reducers'

type PostState = State['info'];
export const getSnackbar = (state: State): PostState['snackbar'] => {
  return state.info.snackbar;
};

export default getSnackbar;
