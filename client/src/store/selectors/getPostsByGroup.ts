import { createSelector, } from 'reselect'
import type { Post } from '../../types/Post';
import type { State } from '../reducers';
import { getPosts, getGroupId } from '../selectors';
import groupBy from "lodash/groupBy";

type PostState = State['blog'];

const groupPosts = (posts: PostState['posts'], groupId: keyof Post) => {
  return groupBy(posts, (post) => {
    return post[groupId];
  });
}

export const getPostsByGroup = createSelector(
  [
    getPosts,
    (state) => getGroupId(state)
  ],
  (posts, groupId) => groupPosts(posts, groupId)
);


export default getPostsByGroup;