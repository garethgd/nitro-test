import { AxiosError } from 'axios';
import axios from 'axios';
import { ActionTypes } from '../actions/constants';
import { postService } from "../../services";
import type { Post } from '../../types/Post'
import { toggleSnackbar } from './infoActions'
export const instanceAxios = axios;

export const getAllPosts = () => async (dispatch: Function) => {
  dispatch({ type: ActionTypes.IS_BUSY, payload: true });
  try {
    const response = await postService.getPosts();
    dispatch({ type: ActionTypes.GET_POSTS, payload: response });
  } catch (err) {
    handleError(err as AxiosError, dispatch);
  }
};

// Edits a Post
export const updatePost = (updatedPost: Post) => async (
  dispatch: Function
) => {
  dispatch({ type: ActionTypes.IS_BUSY, payload: true });
  try {
    const response = await postService.updatePost(updatedPost);
    dispatch({ type: ActionTypes.UPDATE_POST, payload: response });
    dispatch(toggleSnackbar('Posts Updated'));
  } catch (err) {
    handleError(err as AxiosError, dispatch);
  }
};

// Adds a Post
export const createPost = (newPost: Post) => async (
  dispatch: Function
) => {
  dispatch({ type: ActionTypes.IS_BUSY, payload: true });
  try {
    const response = await postService.createPost(newPost);
    dispatch({ type: ActionTypes.CREATE_POST, payload: response });
    dispatch(toggleSnackbar('Post Created'));
  } catch (err) {
    handleError(err as AxiosError, dispatch);
  }
};

// Groups Posts
export const setGroupId = (groupId: keyof Post) => async (
  dispatch: Function
) => {
  dispatch({ type: ActionTypes.SET_GROUP_ID, payload: groupId });
};

export const groupPosts = (groupId: keyof Post) => async (
  dispatch: Function
) => {
  dispatch({ type: ActionTypes.SET_GROUP_ID, payload: groupId });
};

export const toggleAddModal = () => ({
  type: ActionTypes.TOGGLE_ADD_MODAL,
});

const handleError = (error: AxiosError, dispatch: Function) => {
  if (error.response) {
    let message;
    if (typeof error.response.data === 'string') {
      message = error.response.data;
    } else {
      message = error.response.data
        ? error.response.data.messages[0]
        : error.message;
    }

    dispatch({ type: ActionTypes.FAILED_REQUEST, payload: message });
  } else if (error.request) {
    console.log(error.request);
  } else {
    dispatch({ type: ActionTypes.FAILED_REQUEST, payload: error.message });
    console.log('Error', error.message);
  }
};


