import type { Post, SnackbarPayload } from '../../types'
import { AxiosError } from 'axios';
import { ActionTypes } from './constants'

export interface CreatePost {
  type: ActionTypes.CREATE_POST;
  payload: Post[];
}
export interface GetPosts {
  type: ActionTypes.GET_POSTS;
  payload: { posts: Post[] };
}
export interface UpdatePost {
  type: ActionTypes.UPDATE_POST;
  payload: Partial<Post>;
}

export interface ToggleSnackBar {
  type: ActionTypes.TOGGLE_SNACKBAR;
  payload: SnackbarPayload;
}
export interface ToggleAddModal {
  type: ActionTypes.TOGGLE_ADD_MODAL;
}
export interface IsBusy {
  type: ActionTypes.IS_BUSY;
  payload: { isBusy: boolean };
}

export interface FailedRequest {
  type: ActionTypes.FAILED_REQUEST;
  payload: AxiosError;
}

export interface SetGroupId {
  type: ActionTypes.SET_GROUP_ID;
  payload: keyof Post;
}

export type Action =
  | CreatePost
  | GetPosts
  | SetGroupId
  | ToggleAddModal
  | IsBusy
  | UpdatePost
  | FailedRequest
  | ToggleSnackBar