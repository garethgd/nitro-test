import { ActionTypes } from '../constants';
import * as actions from '..';
import configureMockStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

//Testing actions with mock redux store
const mockStore = configureMockStore([thunkMiddleware]);

describe('CRUD Redux Actions', () => {
  let mock;
  beforeEach(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(function () {
    mock.reset();
  });

  const testPosts = [
    {
      "id": 1,
      "location": "San Francisco",
      "time": "1552657573",
      "author": "Happy User",
      "text": "Proper PDF conversion ensures that every element of your document remains just as you left it."
    },
    {
      "id": 2,
      "location": "San Francisco",
      "time": "1552571173",
      "author": "Happy User",
      "text": "The modern workplace is increasingly digital, and workflows are constantly evolving. "
    },
  ];

  it('should handle busy action when using get posts API', () => {
    const newStore = mockStore();
    mock
      .onGet('/posts')
      .reply(200, testPosts);
    newStore.dispatch(actions.getAllPosts());
    const action = newStore.getActions();
    const expectedActions = {
      type: ActionTypes.IS_BUSY,
      payload: true,
    };
    expect(action[0]).toEqual(expectedActions);
  });
  it('should handle get posts API', done => {

    mock
      .onGet('/posts')
      .reply(200, testPosts);

    const expectedActions = [
      { payload: true, type: 'IS_BUSY' },
      {
        type: ActionTypes.GET_POSTS,
        payload: testPosts,
      },
    ];

    const store = mockStore({});

    store.dispatch(actions.getAllPosts()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
      done();
    });
  });


  it('should handle create post API', done => {
    const createdPost = {
      "id": 1,
      "location": "San Francisco",
      "time": "1552657573",
      "author": "Happy User",
      "text": "Proper PDF conversion ensures that every element of your document remains just as you left it."
    };

    mock = new MockAdapter(axios);
    mock
      .onPost(`/posts`)
      .reply(200, createdPost);

    const expectedActions = [
      { payload: true, type: 'IS_BUSY' },
      {
        type: ActionTypes.CREATE_POST,
        payload: createdPost,
      },
      {
        type: ActionTypes.TOGGLE_SNACKBAR,
        payload: { message: 'Post Created' },
      },
    ];

    const store = mockStore({});

    store.dispatch(actions.createPost(createdPost)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
      done();
    });

  });

  it('should handle update post API', done => {
    const editedPost = {
      "id": 1,
      "location": "San Francisco",
      "time": "1552657573",
      "author": "Happy User",
      "text": "Proper PDF conversion ensures that every element of your document remains just as you left it."
    };
    const { id } = editedPost;
    mock = new MockAdapter(axios);

    mock
      .onPut(
        `/posts/${id}`,
        editedPost
      )
      .reply(200, editedPost);

    const expectedActions = [
      { payload: true, type: 'IS_BUSY' },
      {
        type: ActionTypes.UPDATE_POST,
        payload: editedPost,
      },
      {
        type: ActionTypes.TOGGLE_SNACKBAR,
        payload: { message: 'Posts Updated' },
      },
    ];

    const store = mockStore({});

    store.dispatch(actions.updatePost(editedPost)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
      done();
    });

  });

  it('should handle error request', done => {
    // Returns a failed promise with Error('Network Error');
    mock.onGet('/users').networkError();

    const expectedActions = [
      { payload: true, type: 'IS_BUSY' },
      {
        type: ActionTypes.FAILED_REQUEST,
        payload: 'Request failed with status code 404',
      },
    ];

    const store = mockStore({});

    store.dispatch(actions.getAllPosts()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
      done();
    });

  });

  it('should toggle modal state', done => {
    const expectedActions = [
      {
        type: ActionTypes.TOGGLE_ADD_MODAL,
      },
    ];
    const store = mockStore({});
    store.dispatch(actions.toggleAddModal());
    expect(store.getActions()).toEqual(expectedActions);
    done();
  });
});