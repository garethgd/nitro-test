import { ActionTypes } from '../constants';
import * as actions from '..';
import configureMockStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

//Testing actions with mock redux store
const mockStore = configureMockStore([thunkMiddleware]);

describe('CRUD Redux Actions', () => {
  let mock;
  beforeEach(() => {
    mock = new MockAdapter(axios);
  });

  afterEach(function () {
    mock.reset();
  });

  it('should toggle snackbar', done => {
    const expectedActions = [
      {
        type: ActionTypes.TOGGLE_SNACKBAR,
        payload: { message: 'Test Message' }
      },
    ];

    const store = mockStore({});

    store.dispatch(actions.toggleSnackbar('Test Message'));
    expect(store.getActions()).toEqual(expectedActions);
    done();
  });

});