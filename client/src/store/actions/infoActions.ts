import axios from 'axios';
import { ActionTypes } from './constants';
export const instanceAxios = axios;

export const toggleSnackbar = (message?: string) => ({
  type: ActionTypes.TOGGLE_SNACKBAR,
  payload: { message }
});
