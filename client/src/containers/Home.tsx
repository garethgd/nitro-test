import PostsWrapper from "../components/PostWrapper/PostsWrapper";
import { connect } from "react-redux";
import { State as ReducerState } from "../store/reducers";
import {
  createPost,
  groupPosts,
  toggleAddModal,
  getAllPosts,
  toggleSnackbar,
  updatePost,
} from "../store/actions";
import type { Post, GroupedPosts } from "../types/Post";
import Snackbar from "../components/Snackbar/Snackbar";
import {
  getPostsByGroup,
  getIsAddModalOpen,
  getSnackbar,
  getIsLoading,
  getGroupId,
} from "../store/selectors";
import Modal from "../components/Modal/Modal";
import { LinearProgress } from "@mui/material";
export type State = {};

export type Props = {
  posts: GroupedPosts;
  info: ReducerState["info"];
  snackbar: {
    message: string;
    open: boolean;
  };
  onPostAdd: (post: Post) => void;
  toggleAddModal: () => void;
  onGroupPost: (post: keyof Post) => void;
  onToggleSnackbar: () => void;
  onPostEdit: (post: Post) => void;
  onGetAllPosts: () => void;
  isLoading: boolean;
  groupId: keyof Post;
  isAddModalOpen: boolean;
};

const mapStateToProps = (state: ReducerState) => ({
  posts: getPostsByGroup(state),
  isAddModalOpen: getIsAddModalOpen(state),
  groupId: getGroupId(state),
  snackbar: getSnackbar(state),
  isLoading: getIsLoading(state),
});

// wrapped CRUD operations with dispatch for better testing on actions

const mapDispatchToProps = (dispatch: Function) => {
  return {
    onPostAdd: (post: Post) => dispatch(createPost(post)),
    onGroupPost: (groupId: keyof Post) => dispatch(groupPosts(groupId)),
    toggleAddModal: () => dispatch(toggleAddModal()),
    onToggleSnackbar: () => dispatch(toggleSnackbar()),
    onPostEdit: (post: Post) => dispatch(updatePost(post)),
    onGetAllPosts: () => dispatch(getAllPosts()),
  };
};

const App = (props: Props) => {
  const {
    posts,
    onToggleSnackbar,
    snackbar,
    onGroupPost,
    onGetAllPosts,
    toggleAddModal,
    groupId,
    onPostEdit,
    onPostAdd,
    isAddModalOpen,
    isLoading,
  } = props;

  return (
    <div className="App">
      <header>{isLoading && <LinearProgress />}</header>
      <PostsWrapper
        onToggleSnackbar={onToggleSnackbar}
        onGroupPost={onGroupPost}
        toggleAddModal={toggleAddModal}
        groupId={groupId}
        getPosts={onGetAllPosts}
        posts={posts}
        onEditPost={onPostEdit}
        isLoading={isLoading}
        onPostAdd={onPostAdd}
      />
      <Modal
        open={isAddModalOpen}
        toggleAddModal={toggleAddModal}
        onPostAdd={onPostAdd}
      />
      <Snackbar onToggleSnackbar={onToggleSnackbar} snackbar={snackbar} />
    </div>
  );
};

export default connect<any, any, any>(
  mapStateToProps as any,
  mapDispatchToProps
)(App);
