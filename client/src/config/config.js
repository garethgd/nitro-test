
export const config = {
  api: {
    url: process.env.NODE_ENV === 'production' ? process.env.REACT_APP_API_URL : '',
  },
};
