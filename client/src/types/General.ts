export type SnackbarMessage = {
  message: string,
  open: boolean
};

export type SnackbarPayload = {
  message?: string,
};