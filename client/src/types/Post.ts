import type { Dictionary } from 'lodash';
export interface Post {
  id: number;
  location: string;
  time: string;
  author: string;
  text: string;
  week?: number;
}
export interface GroupedPosts {
  posts: Dictionary<[Post, ...Post[]]>
}
