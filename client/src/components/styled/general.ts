import styled from '@emotion/styled';
import ModalUnstyled from '@mui/base/ModalUnstyled';
type ButtonProps = {
  primary?: boolean;
};

export const Button = styled.button<ButtonProps>`
  background: ${(props) => props.primary ? '#efd368' : '#c5b883'};
  padding: 10px 30px;
  border: none;
  cursor: pointer;
  text-transform: uppercase;
  letter-spacing: 0.5px;
  max-height: 30px;
  opacity: 1;
  font-size: 10px;
  font-weight: bold;
  margin: 10px;


  &:hover {
    transition: all 0.2s ease;
    transition-property: transform, opacity, color, border-color,
      background-color;
    opacity: 0.8;
  }
`

export const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Backdrop = styled('div')`
  z-index: -1;
  position: fixed;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.5);
  -webkit-tap-highlight-color: transparent;
`;

export const FormContainer = styled('div')`
  width: 100%:
`;

export const Error = styled('div')`
  color: red;
  marging: 5px;
`;

export const PostsContainer = styled('div')`
  > div {
    height: 100%;
  }
  h1 {
    font-size: 20px;
  }
`;

export const Actions = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 5px;
`;

export const PlusIconWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: auto;
  cursor: pointer;
  &:hover {
    opacity: 0.6
  }
  svg {
    margin-right: 10px;
  }
`;

export const EditForm = styled('form')`
  display: flex;
  padding: 20px;
  justify-content: center;
  border-bottom: 1px solid #dddddd;
`;

export const Post = styled('div')`
  flex: 1 0 15%;
  height: 400px;
  padding: 20px;
  display: flex;
  justify-content: center;
  margin: 10px;
  flex-direction: column;
  text-align: center;
  opacity: 1;
`;



