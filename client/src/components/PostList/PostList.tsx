import React, { FunctionComponent } from "react";
import Post from "../Post/Post";
import EditPostForm from "../EditPostForm/EditPostForm";
import { Post as PostType, GroupedPosts } from "../../types/Post";
import TreeView from "@mui/lab/TreeView";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import TreeItem from "@mui/lab/TreeItem";

const PostList: FunctionComponent<{
  posts: GroupedPosts;
  isLoading: boolean;
  groupId: keyof PostType;
  onEditPost: (post: PostType) => void;
}> = ({ posts, onEditPost, isLoading, groupId }) => {
  const formatKey = (key: string) =>
    groupId === "week" ? `Week: ${key}` : key;

  const renderTree = () => {
    return Object.entries(posts).map(([key, post], i) => {
      return (
        <TreeItem
          key={(i * Math.random()).toString()}
          nodeId={i.toString()}
          label={`${formatKey(key)}`}
        >
          {post.map((p: PostType, j: number) => {
            const { time } = p;
            return (
              <React.Fragment key={parseInt(time) * j}>
                <Post post={p} />
                <EditPostForm
                  isLoading={isLoading}
                  onEditPost={onEditPost}
                  post={p}
                />
              </React.Fragment>
            );
          })}
        </TreeItem>
      );
    });
  };

  return (
    <TreeView
      aria-label="file system navigator"
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      sx={{
        flexGrow: 0.8,
        overflowY: "auto",
        height: "100%",
        overflowX: "hidden",
      }}
    >
      {renderTree()}
    </TreeView>
  );
};

export default PostList;
