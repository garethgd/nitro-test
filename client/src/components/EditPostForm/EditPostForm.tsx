import { Box } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { useFormik } from "formik";
import { isEqual } from "lodash";
import React from "react";
import * as yup from "yup";
import type { Post } from "../../types/Post";
import { Button, EditForm } from "../styled/general";

const formBoxStyle = {
  display: "flex",
};
export type Props = {
  post: Post;
  onEditPost: (post: Post) => void;
  isLoading: boolean;
};

const validationSchema = yup.object({
  author: yup.string().required("Author is required"),
  location: yup.string().required("Location is required"),
});

const EditPostForm = ({ post, onEditPost, isLoading }: Props) => {
  const { author, location } = post;
  const formik = useFormik({
    initialValues: { location: location, author: author },
    validationSchema: validationSchema,
    onSubmit: (values, helpers) => {
      const initialVales = { location: location, author: author };
      if (isEqual(values, initialVales)) {
        helpers.setErrors({
          author: "No change has been made",
          location: "No change has been made",
        });
        return;
      }
      const updatedPost: Post = {
        ...post,
        location: values.location,
        author: values.author,
      };
      onEditPost(updatedPost);
    },
  });

  return (
    <div>
      <EditForm onSubmit={formik.handleSubmit}>
        <Box sx={formBoxStyle}>
          <TextField
            fullWidth
            id="author"
            name="author"
            label="Author"
            style={{ marginRight: "10px" }}
            value={formik.values.author}
            onChange={formik.handleChange}
            error={formik.touched.author && Boolean(formik.errors.author)}
            helperText={formik.touched.author && formik.errors.author}
          />

          <TextField
            fullWidth
            id="location"
            name="location"
            label="Location"
            value={formik.values.location}
            onChange={formik.handleChange}
            error={formik.touched.location && Boolean(formik.errors.location)}
            helperText={formik.touched.location && formik.errors.location}
          />
        </Box>

        <Button disabled={isLoading}>Save</Button>
      </EditForm>
    </div>
  );
};

export default EditPostForm;
