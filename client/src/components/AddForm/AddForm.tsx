import { Box } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { useFormik } from "formik";
import React from "react";
import * as yup from "yup";
import type { Post } from "../../types/Post";
import { Button, FormContainer } from "../styled/general";

export type Props = {
  onPostAdd: (post: Post) => void;
  toggleAddModal: () => void;
};
const boxStyle = {
  display: "flex",
  justifyContent: "end",
};

const formBoxStyle = {
  display: "flex",
  flexDirection: "column",
};

const validationSchema = yup.object({
  author: yup.string().required("Author is required"),
  location: yup.string().required("Location is required"),
  time: yup.string().required("Time is required"),
  text: yup
    .string()
    .min(40, "Text should be of minimum 40 characters length")
    .required("Text is required"),
});

const AddForm = ({ onPostAdd, toggleAddModal }: Props) => {
  const formik = useFormik({
    initialValues: { location: "", author: "", time: "", text: "" },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      const id = Math.floor(Math.random() * 50);
      const { time } = values;
      const updatedValues = {
        ...values,
        id,
        time: new Date(time).getTime().toString(),
      };
      onPostAdd(updatedValues);
    },
  });

  return (
    <FormContainer>
      <form onSubmit={formik.handleSubmit}>
        <h2>Add a Post</h2>
        <Box sx={formBoxStyle}>
          <TextField
            fullWidth
            id="author"
            name="author"
            label="Author"
            value={formik.values.author}
            onChange={formik.handleChange}
            error={formik.touched.author && Boolean(formik.errors.author)}
            helperText={formik.touched.author && formik.errors.author}
          />

          <TextField
            fullWidth
            id="location"
            name="location"
            label="Location"
            value={formik.values.location}
            onChange={formik.handleChange}
            error={formik.touched.location && Boolean(formik.errors.location)}
            helperText={formik.touched.location && formik.errors.location}
          />
          <TextField
            fullWidth
            id="text"
            name="text"
            label="Text"
            value={formik.values.text}
            onChange={formik.handleChange}
            error={formik.touched.text && Boolean(formik.errors.text)}
            helperText={formik.touched.text && formik.errors.text}
          />
          <TextField
            fullWidth
            id="time"
            type="date"
            name="time"
            label="Time"
            value={formik.values.time}
            onChange={formik.handleChange}
            error={formik.touched.time && Boolean(formik.errors.time)}
            helperText={formik.touched.time && formik.errors.time}
          />
        </Box>
        <Box sx={boxStyle}>
          <Button primary>Add</Button>
          <Button onClick={toggleAddModal}>Cancel</Button>
        </Box>
      </form>
    </FormContainer>
  );
};

export default AddForm;
