import React from "react";
import { render, screen } from "@testing-library/react";
import PostsWrapper from "../PostsWrapper";

const mockGetPosts = jest.fn();

test('render no posts message if no posts', () => {
  render(<PostsWrapper posts={undefined} getPosts={mockGetPosts} />);
  const emptyPostsMessage = screen.queryByText(/No posts available/i);
  expect(mockGetPosts).toHaveBeenCalledTimes(1);
  expect(emptyPostsMessage).toBeInTheDocument();
});

test('render no posts message if posts are empty', () => {
  render(<PostsWrapper posts={[]} getPosts={mockGetPosts} />);
  const emptyPostsMessage = screen.queryByText(/No posts available/i);
  expect(mockGetPosts).toHaveBeenCalledTimes(1);
  expect(emptyPostsMessage).toBeInTheDocument();
});

test('do not show empty posts message if app is loading', () => {
  render(<PostsWrapper posts={[]} getPosts={mockGetPosts} isLoading={true} />);
  const emptyPostsMessage = screen.queryByText(/No posts available/i);
  expect(mockGetPosts).toHaveBeenCalledTimes(1);
  expect(emptyPostsMessage).toBeNull();
});
