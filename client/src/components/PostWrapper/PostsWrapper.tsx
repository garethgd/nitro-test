import { isEmpty } from "lodash";
import React, { useEffect } from "react";
import { GroupedPosts, Post } from "../../types/Post";
import ActionButtons from "../ActionButtons/ActionButtons";
import PostList from "../PostList/PostList";
import { PostsContainer } from "../styled/general";

export type Props = {
  posts: GroupedPosts;
  isLoading: boolean;
  groupId: keyof Post;
  onToggleSnackbar: () => void;
  toggleAddModal: () => void;
  onGroupPost: (groupId: keyof Post) => void;
  onEditPost: (post: Post) => void;
  onPostAdd: (post: Post) => void;
  getPosts: () => void;
};

const PostsWrapper = (props: Props) => {
  const {
    getPosts,
    posts,
    onEditPost,
    onGroupPost,
    toggleAddModal,
    groupId,
    isLoading,
  } = props;

  useEffect(() => {
    const initFetch = async () => {
      try {
        await getPosts();
      } catch (err) {
        console.log(err);
      }
    };
    initFetch();
  }, [getPosts]);

  return (
    <PostsContainer>
      <ActionButtons
        onGroupPost={onGroupPost}
        toggleAddModal={toggleAddModal}
      />
      <div>
        {isEmpty(posts) && !isLoading ? (
          <h1>No posts available</h1>
        ) : (
          <div>
            {
              <PostList
                isLoading={isLoading}
                groupId={groupId}
                onEditPost={onEditPost}
                posts={posts}
              />
            }
          </div>
        )}
      </div>
    </PostsContainer>
  );
};

export default PostsWrapper;
