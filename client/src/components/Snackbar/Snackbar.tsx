import Snackbar from "@material-ui/core/Snackbar";

type Props = {
  snackbar: {
    message: string;
    open: boolean;
  };
  onToggleSnackbar: () => void;
};

const SnackbarComponent = ({ snackbar, onToggleSnackbar }: Props) => {
  const { message, open } = snackbar;
  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      style={{ height: "auto" }}
      onClose={onToggleSnackbar}
      open={open}
      autoHideDuration={6000}
      ContentProps={{
        "aria-describedby": "message-id",
      }}
      message={<span id="message-id"> {message}</span>}
    />
  );
};

export default SnackbarComponent;
