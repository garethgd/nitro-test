import { Box } from "@mui/system";
import type { Post } from "../../types/Post";
import AddForm from "../AddForm/AddForm";
import { Backdrop, StyledModal } from "../styled/general";
const style = {
  width: 400,
  bgcolor: "white",
  border: "2px solid #000",
  p: 2,
  px: 4,
  pb: 3,
};

type Props = {
  toggleAddModal: () => void;
  onPostAdd: (post: Post) => void;
  open: boolean;
};

const Modal = ({ toggleAddModal, open, onPostAdd }: Props) => {
  return (
    <div>
      <StyledModal
        aria-labelledby="unstyled-modal-title"
        aria-describedby="unstyled-modal-description"
        open={open}
        onClose={toggleAddModal}
        BackdropComponent={Backdrop}
      >
        <>
          <Box sx={style}>
            <AddForm onPostAdd={onPostAdd} toggleAddModal={toggleAddModal} />
          </Box>
        </>
      </StyledModal>
    </div>
  );
};

export default Modal;
