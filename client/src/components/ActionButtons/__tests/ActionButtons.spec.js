import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ActionButtons from "../ActionButtons";
const mockSetGroup = jest.fn();
const mockToggleAddModal = jest.fn();

test('ensure add post button fires toggle', () => {
  render(<ActionButtons toggleAddModal={mockToggleAddModal} onGroupPost={mockSetGroup} />);
  const addPostButton = screen.queryByText(/Add Post/i);
  fireEvent.click(addPostButton)
  expect(mockToggleAddModal).toHaveBeenCalledTimes(1);
});

test('ensure author grouping post button fires toggle', () => {
  render(<ActionButtons toggleAddModal={mockToggleAddModal} onGroupPost={mockSetGroup} />);
  const groupByWeekBtn = screen.queryByText(/Group by Author/i);
  fireEvent.click(groupByWeekBtn)
  expect(mockSetGroup).toHaveBeenCalledTimes(1);
  expect(mockSetGroup).toHaveBeenCalledWith('author');
});

test('ensure location grouping post button fires toggle', () => {
  render(<ActionButtons toggleAddModal={mockToggleAddModal} onGroupPost={mockSetGroup} />);
  const groupByWeekBtn = screen.queryByText(/Group by Location/i);
  fireEvent.click(groupByWeekBtn)
  expect(mockSetGroup).toHaveBeenCalledTimes(1);
  expect(mockSetGroup).toHaveBeenCalledWith('location');
});

test('ensure week grouping post button fires toggle', () => {
  render(<ActionButtons toggleAddModal={mockToggleAddModal} onGroupPost={mockSetGroup} />);
  const groupByWeekBtn = screen.queryByText(/Group by Week/i);
  fireEvent.click(groupByWeekBtn)
  expect(mockSetGroup).toHaveBeenCalledTimes(1);
  expect(mockSetGroup).toHaveBeenCalledWith('week');
});
