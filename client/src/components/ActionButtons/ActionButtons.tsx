import { FaPlus } from "react-icons/fa";
import { Actions, Button, PlusIconWrapper } from "../styled/general";
import React from "react";
import type { Post } from "../../types/Post";

type Props = {
  toggleAddModal: () => void;
  onGroupPost: (groupId: keyof Post) => void;
};

const ActionButtons = React.memo(({ toggleAddModal, onGroupPost }: Props) => {
  return (
    <Actions>
      <PlusIconWrapper onClick={() => toggleAddModal()}>
        <FaPlus id="create-post-btn" />
        Add Post
      </PlusIconWrapper>
      <Button primary onClick={() => onGroupPost("week")}>
        Group by Week
      </Button>
      <Button primary onClick={() => onGroupPost("author")}>
        Group by Author
      </Button>
      <Button onClick={() => onGroupPost("location")}>Group by Location</Button>
      {/* Hidden div for alignment */}
      <div style={{ marginRight: "auto" }}></div>
    </Actions>
  );
});

export default ActionButtons;
