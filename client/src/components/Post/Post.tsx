import { Post as PostType } from "../../types/Post";
import { Post as StyledPost } from "../styled/general";

export type Props = {
  post: PostType;
};

const Post = ({ post }: Props) => {
  const { author, text, time, location } = post;
  const formattedDate = new Date(parseInt(time)).toLocaleDateString("en-us", {
    weekday: "long",
    year: "numeric",
    month: "short",
    day: "numeric",
  });

  return (
    <StyledPost>
      <div className="info">
        {location}
        <h2>{author}</h2>
        <p>{text}</p>
        {<span>{time ? formattedDate : "Unable to get date"}</span>}
      </div>
    </StyledPost>
  );
};

export default Post;
