import { render, screen } from "@testing-library/react";
import Post from "../Post";

test('ensure post information is in dom', () => {
  const samplePost = {
    location: 'Dublin',
    author: 'Joyce',
    text: 'Tesing Text',
    time: 123466,
    id: 5
  };
  const { location, author, text } = samplePost;
  render(<Post post={samplePost} />);
  const locationText = screen.queryByText(`${location}`);
  const authorText = screen.queryByText(`${author}`);
  const postText = screen.queryByText(`${text}`);
  const timeText = screen.getByText('Thursday, Jan 1, 1970');
  expect(locationText).toBeInTheDocument();
  expect(authorText).toBeInTheDocument();
  expect(postText).toBeInTheDocument();
  //ensure date is transform
  expect(timeText).toBeInTheDocument();
});

