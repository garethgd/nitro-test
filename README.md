# Gareth Dunne - Nitro Test

# Instructions

In this project, there is both an express server folder and a client folder with separate dependencies.

## Server

Navigate to the server folder from root:

    cd server
    
Create a `.env` file in `/server` with the following information:

    PORT=8080

Install dependencies:

    npm i
Start server:

    npm run dev

## Client

    cd client
Install dependencies:

    npm i
Start client:

    npm run start



After both server and client started navigate to: http://localhost:3000/

## Testing
There are 23 unit tests in the project. The majority of these are around the reducers and actions of the Redux store.

To test them out, navigate to the client:

    cd client
    
And run the command:

    npm run test

## Considerations

### Server
The server is a standard [Express.js](https://expressjs.com/) implementation with multiple plugins and middlewares to enhance its routes, requests and logging.

### State Management
For state management, I considered using something like [React Context](https://reactjs.org/docs/context.html) to manage the small amount of data in the app. However, I decided against it and wanted to fully build out a [Redux](https://redux.js.org/) store with actions, reducers and selectors. I felt like this showed how the app would scale if more time was available. Additionally, the majority of the state logic is in the Redux store. This means a lot of the potential controlled state from components was lifted up and sent down from the store via prop drilling.

### TypeScript
TypeScript also came with built-in with Create React App. It was extremely helpful while mapping the Post data to the UI. It also allowed me to see how the grouped post data structure can work.

### Data
As there was no time to create a fully fledged database. I enabled my routes to read and write post data from a local file (`sampleData.json`). I thought this was the most time efficient way to simulate CRUD operations. However, MongoDB and mongoose would be very easy to implement into the existing structure.

### Form Management
I opted to use [Formik](https://formik.org/) for handling the creating and editing of post forms. I found its very straightforward to use and allows for very flexible field validation.

### Styling
I reached for Material UI and [Emotionjs](https://emotion.sh/docs/introduction) for styling and form components. This allowed for consistent styles throughout the app's components.

### Testing
The latest version of Create React App comes with [react testing library](https://testing-library.com/docs/react-testing-library/intro/) by default. This is my preference for creating unit tests in the frontend. The API is much cleaner for targeting user behaviors rather than searching for nodes inside the UI. For years I would be frustrated with targeting UI nodes using Enzyme.

As most of the state logic resides within the actions and reducers, this has the most comprehensive unit tests. There are also **react testing library** unit tests for the dumb components.

### Backend Validation
To validate the data in requests to the server, I reached for [Joi](https://joi.dev/api/) to create a schema with shape acceptance criterias. I found it relatively straightforward to map out accepted data types.

    const createPost = Joi.object(
    { 
      location: Joi.string(), 
	  author: Joi.string(), 
	  id: Joi.number(), 
	  time: Joi.number(), 
	  text: Joi.string()
	 });

For setting security headers, I wrapped the app with [helmet](https://github.com/helmetjs/helmet)

    app.use(helmet());

## Grouping by ID

Grouping the data seemed to be the focus of the test. I found that this was the most expensive computation of the sample data.  As a result, I wanted to make sure that this isn't recalculated unnecessarily. To cache the result of the grouping logic, I used the [reselect library](https://github.com/reduxjs/reselect). This allowed me to only recalculate the grouping structure once its input (groupId) was changed to a new value. ('week', 'location, 'author')

    export  const  getPostsByGroup  =  createSelector(
    [
	    getPosts,
	    (state)  =>  getGroupId(state)
    ],
    (posts,  groupId)  =>  groupPosts(posts,  groupId)

## Decorating by week
In the backend, I decorated the post data with an additional `week` property. This allowed the frontend to render the grouped week labels correctly:

    // Utility to add a week value to post based on date timestamp
   
    const  decorateWithWeek = (posts)  =>  {
	    let sortedPosts = [];
	    
	    sortedPosts = posts.sort((x, y)  =>  {
		    return  x.time - y.time;
	    })
	    
	    sortedPosts.forEach(p =>  {
	    
	    p.week = dayjs(p.time).week();
	    })
	    return sortedPosts;
    };

## Additional Time
With more time, I would like to implement the following improvements:

 - A Mongoose connection to a MongoDB database with a Post Schema.
 - JWT session management for requests/responses using something like [passport jwt].(https://www.npmjs.com/package/passport-jwt)
 - More unit tests for the server. The majority of tests were in the frontend as the logic was happening in Redux state.
 - Additional data validation using Joi.
 - More specific error handling for a range of error cases.
 - More detailed server side logging using Winston and Morgan.
 - Advanced [server side](https://redux.js.org/usage/server-rendering) rendering using Redux.




